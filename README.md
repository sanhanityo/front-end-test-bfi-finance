Frontend Test
===================
Front End Developer test for BFI Finance
>Live demo https://gallant-darwin-d957a3.netlify.com/
>Untuk preview file yang sudah di build bisa lihat di directory **/dist**.

### Installation
Pastikan anda memiliki Node.js v8+
Install dependencies, devDependencies.

```sh
$ git clone https://bitbucket.org/sanhanityo/front-end-test-bfi-finance.git
$ cd front-end-test-bfi-finance
$ npm install
```

### Development
```sh
$ npm run watch // otomatis membuka browser dengan url http://localhost:3000
```

### Build file
```sh
$ npm run build // build file ke directory /dist
```

### Library
  - Bootstrap Framework v4
  - @chenfengyuan/datepicker