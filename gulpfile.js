const del = require('del'),
    gulp = require('gulp'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    cleanCss = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync'),
    bro = require('gulp-bro'),
    nunjucks = require('gulp-nunjucks');

/**
 * Base Directory
 */
const baseDirs = {
    src: 'src/',
    dist: 'dist/'
};

/**
 * Paths project
 */
const paths = {
    styles: {
        dev: `${baseDirs.src}scss/style.scss`,
        dist: `${baseDirs.dist}assets/styles`,
        scss: `${baseDirs.src}scss/**/*.scss`,
    },
    scripts: {
        dev: `${baseDirs.src}scripts/app.js`,
        dist: `${baseDirs.dist}assets/scripts`,
    },
    templates: {
        html: `${baseDirs.src}template/*.html`,
        layouts: `${baseDirs.src}template/layout/*.html`,
    },
    images: {
        file: `${baseDirs.src}/images/*`,
        optimize: `${baseDirs.dist}/assets/images`
    },
    font: {
        file: `${baseDirs.src}/fonts/*`,
        dist: `${baseDirs.dist}/assets/styles/fonts/`
    }
};

/**
 * Clean
 * 
 * Delete all file at dist directory
 */
gulp.task('clean', () => {
    del.sync([baseDirs.dist]);
});

/**
 * Scss
 * 
 * Compile .scss to .css and minify file
 */
gulp.task('scss', () => {
    return gulp.src(paths.styles.dev)
        .pipe(sass({
            includePaths: ['node_modules']
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCss())
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest(paths.styles.dist))
        .pipe(browserSync.stream());
});

/**
 * Script
 * 
 * Bundle .js file and minify file
 */
gulp.task('script', () => {
    return gulp.src(paths.scripts.dev)
        .pipe(bro())
        .pipe(uglify())
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest(paths.scripts.dist))
        .pipe(browserSync.stream());
});

/**
 * Template
 * 
 * Compile nunjucks file to html
 */
gulp.task('template', () => {
    return gulp.src([paths.templates.html, '!' + paths.templates.layouts])
        .pipe(nunjucks.compile())
        .pipe(gulp.dest(baseDirs.dist))
        .pipe(browserSync.stream());
});

/**
 * Images
 * 
 * Compress file size of images
 */
gulp.task('images', () => {
    return gulp.src(paths.images.file)
        .pipe(gulp.dest(paths.images.optimize));
});

/**
 * Font
 * 
 * Copy Font
 */
gulp.task('font', () => {
    return gulp.src(paths.font.file)
        .pipe(gulp.dest(paths.font.dist));
});

/**
 * Build Task
 */
gulp.task('build', ['clean', 'scss', 'script', 'template', 'images', 'font']);

/**
 * Watching file
 */
gulp.task('watching', () => {
    browserSync.init({
        server: baseDirs.dist
    });

    gulp.watch(paths.styles.scss, ['scss']);
    gulp.watch(paths.scripts.dev, ['script']);
    gulp.watch([paths.templates.html, paths.templates.layouts], ['template']);
});

/**
 * Watch Task
 */
gulp.task('watch', ['build', 'watching']);

/**
 * Default Task
 */
gulp.task('default', () => {
    gulp.start('build');
});